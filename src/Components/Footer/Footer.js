import React from 'react';
import BackwardArrow from '../../assets/images/prev_btn.png';
import ForwardArrow from '../../assets/images/nxt_btn.png'
import './Footer.css'

const Footer = () => {
  return (
    <>
       <div className='footer-container'>
            <div className='btn-container'>
                <img alt="Prev btn" src={BackwardArrow} height='50px' width='50px'/>
                <img alt="Next btn" src={ForwardArrow} height='50px' width='50px'/>
            </div>
       </div>
    </>
  )
}

export default Footer
