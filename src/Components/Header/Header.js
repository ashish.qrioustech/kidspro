import React from "react";
import HelpOutlineTwoToneIcon from '@mui/icons-material/HelpOutlineTwoTone';
import "./Header.css";
import { AntSwitch } from "../StyledComponent/StyledComponents";

const Header = () => {
  return (
    <div className="header-container">
      <div className="inner-container">
        <p className="lifeCycle-text">Life Cycle Frog</p> 
          <div className="text-container">
            <span className="Eng-text">Eng</span>
            <span className="toggle-btn">
              <AntSwitch
                defaultChecked
                inputProps={{ "aria-label": "ant design" }}
              />
            </span>
            <span className="Esp-text">Esp</span>
            <HelpOutlineTwoToneIcon className="helpout-line"/>
          </div>
      </div>
    </div>
  );
};

export default Header;
