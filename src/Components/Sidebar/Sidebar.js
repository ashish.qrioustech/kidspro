import React from 'react';
import CameraAltOutlinedIcon from '@mui/icons-material/CameraAltOutlined';
import ZoomInOutlinedIcon from '@mui/icons-material/ZoomInOutlined';
import LibraryBooksOutlinedIcon from '@mui/icons-material/LibraryBooksOutlined';
import './Sidebar.css';

const Sidebar = () => {
  return (
    <div className='sidebar-container'>
      <div className='icon-container'>
        <CameraAltOutlinedIcon/>
        <ZoomInOutlinedIcon/>
        <LibraryBooksOutlinedIcon/>
      </div>
    </div>
  )
}

export default Sidebar
