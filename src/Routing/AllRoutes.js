import { Route, Routes } from "react-router-dom";
import React from 'react';
import HomePage from "../Pages/HomePage/HomePage";
import Hotspots from "../Pages/Hotspots/Hotspots";
import SelectionFrog from "../Pages/SelectionFrog/SelectionFrog";
import FrogVideoPage from "../Pages/FrogVideoPage/FrogVideoPage";
import FrogLabelPage from "../Pages/FrogLabelPage/FrogLabelPage";
import LifecycleFrog from "../Pages/LifecycleFrog/LifecycleFrog";
import BuildLifecycle from "../Pages/BuildLifecycle/BuildLifecycle";

const AllRoutes = () => {
  return (
    <>
        <Routes>
            <Route element={<HomePage/>} path="/"/>
            <Route element={<Hotspots/>} path="/hotspots"/>
            <Route element={<SelectionFrog/>} path="/selectionfrog"/>
            <Route element={<FrogVideoPage/>} path="/frogvideo"/>
            <Route element={<FrogLabelPage/>} path="/froglabelpage"/>
            <Route element={<LifecycleFrog/>} path="/lifecyclepage"/>
            <Route element={<BuildLifecycle/>} path="/buildlifecycle"/>
        </Routes>
    </>
  )
}

export default AllRoutes
