import React from 'react';
import LifeCycle_img from '../../assets/images/lifecycle_Frog_and_butterfly_1new.png';
import Frog_icon from '../../assets/images/frog_icon.png';
import './SelectionFrog.css';
import Header from '../../Components/Header/Header';
import Sidebar from '../../Components/Sidebar/Sidebar';
import Footer from '../../Components/Footer/Footer';

const SelectionFrog = () => {
    
  return (
    <>
    <Header/>        
    <div className='selection-image-container'>
     <Sidebar/>
     <div className='sel-image-container'>
      <img alt="Life Cycle frog" className='sel-life-cycle-img' src={LifeCycle_img} />
      <div className='sel-caption-container'>
      <span>
       Every organism goes through several stages to become and adult.Let's explore.....
      </span>
      <div className='sel-frog-btn-wrapper'>
        <button className='sel-frog-btn'><img alt='frog' src={Frog_icon} className='frog-icon'  height='16px' width='16px'/>Frog</button>
      </div>
      </div>
      <Footer/>
     </div>
    </div>
    </>
  )
}

export default SelectionFrog
