import React from "react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import first_screen from "../../assets/images/first_screen.png";
import "./HomePage.css";
import { modalStyle } from "../../Components/StyledComponent/StyledComponents";
import { useNavigate } from "react-router-dom";

const HomePage = () => {
  const [open, setOpen] = React.useState(true);
  const navigate = useNavigate()
  const handleClose = () => setOpen(false);
  const handleStart = () =>{
    navigate('/hotspots');
  }
  return (
    <>
      <div first-screen-container>
        <div className="first-img-wrapper">
          <img
            alt="first-screen"
            src={first_screen}
            height="200px"
            width="300px"
          />
          <p className="life-cycle-text">Life Cycle: Frog and Butterfly</p>
          <button className="start-btn" onClick={handleStart}>Start</button>
        </div>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={modalStyle}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Text in a modal
            </Typography>
            <Typography id="modal-modal-description" sx={{ mt: 2 }}>
              Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
            </Typography>
            </Box>
        </Modal>
      </div>
    </>
  );
};

export default HomePage;
