import React from 'react';
import LifeCycle_img from '../../assets/images/Lifecycle_Frog_prev.png';
import Play_btn from '../../assets/images/bi_play-circle.png';
import './FrogLabelPage.css';
import Header from '../../Components/Header/Header';
import Sidebar from '../../Components/Sidebar/Sidebar';
import Footer from '../../Components/Footer/Footer';

const FrogLabelPage = () => {
    
  return (
    <>
    <Header/>        
    <div className='label-image-container'>
     <Sidebar/>
     <div className='labelfrog-image-container'>
      <img alt="Life Cycle frog" className='labelfrog-life-cycle-img' src={LifeCycle_img} />
      <div className='label-palay-btn-wrapper'>
        <img alt='label' src={Play_btn}  height='40px' width='40px' className='label-play-btn'/>
      </div>
      <Footer/>
     </div>
    </div>
    </>
  )
}

export default FrogLabelPage
