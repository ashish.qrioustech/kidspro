import React from 'react';
import LifeCycle_img from '../../assets/images/lifecycle_Frog_and_butterfly_1new.png';
import './Hotspots.css';
import Header from '../../Components/Header/Header.js';
import Footer from '../../Components/Footer/Footer.js';
import Sidebar from '../../Components/Sidebar/Sidebar.js';


const Hotspots = () => {
  return (
    <>
    <Header/>        
    <div className='hotspot-image-container'>
     <Sidebar/>
     <div className='image-container'>
      <img alt="Life Cycle frog" className='life-cycle-img' src={LifeCycle_img} />
      <div className='caption-container'>
      <span>
        Do you know that some adult organisms do not look like their? Select the hotspot <div className='pin'></div> to begin exploring.
      </span>
      </div>
      <Footer/>
     </div>
    </div>
    </>
  )
}

export default Hotspots
