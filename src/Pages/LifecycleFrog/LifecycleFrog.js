import React from "react";
import LifeCycle_img from "../../assets/images/frog_life_cycle.png";
import "./LifecycleFrog.css";
import Header from "../../Components/Header/Header";
import Sidebar from "../../Components/Sidebar/Sidebar";
import Footer from "../../Components/Footer/Footer";

const LifecycleFrog = () => {
  return (
    <>
      <Header />
      <div className="lifecycle-container">
        <Sidebar />
        <div className="lifecycle-wrapper">
          <div className="lifecycle-img-wrapper">
            <p className="lifecycle-text">Life cycle of Frog</p>
            <img
              alt="Life Cycle frog"
              src={LifeCycle_img}
              height='400px'
              width='450px'
            />
          </div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default LifecycleFrog;
