import React from "react";
import LifeCycle_img from "../../assets/images/Cycle_diagram.svg";
import AdultFrog from "../../assets/images/adult_frog.png";
import Tadepole_04_legs from "../../assets/images/tadepole_04_legs.png";
import Tadepole_02_legs from "../../assets/images/tadepole_02_legs.png";
import Tadepole from "../../assets/images/tadepole.png";
import Egg from "../../assets/images/eggs.png";
import "./BuildLifecycle.css";
import Header from "../../Components/Header/Header";
import Sidebar from "../../Components/Sidebar/Sidebar";
import Footer from "../../Components/Footer/Footer";

const BuildLifecycle = () => {
  const imagesObj = [
    {
        id:1,
        url:AdultFrog
    },
    {
        id:2,
        url:Tadepole_02_legs
    },
    {
        id:3,
        url:Tadepole_04_legs
    },
    {
        id:4,
        url:Egg
    },
    {
        id:5,
        url:Tadepole
    }
]
  return (
    <>
      <Header />
      <div className="buildlifecycle-container">
        <Sidebar />
        <div className="buildlifecycle-wrapper">
          <div className="buildlifecycle-img-wrapper">
          <div className="dropable-area">
            <p className="buildlifecycle-text">Build the Life cycle of a Frog</p>
            <img
              alt="Life Cycle frog"
              src={LifeCycle_img}
              height='450px'
              width='500px'
            />
          </div>
          <div className="dragable-area">
            <p className="dragable-area-text">Drag and drop the stages into the correct locations to complete the frog's life cycle.</p>
            <div className="drag-img-container">
            {
                imagesObj.map((Url)=>{
                    return(
                        <div key={Url.id} className='drag-img-wrapper'><img alt="logo" src={Url.url} height='80px' width='80px'/></div>
                    )
                })
            }
            </div>
          </div>
          </div>
          <Footer/>
        </div>
      </div>
    </>
  );
};

export default BuildLifecycle;
