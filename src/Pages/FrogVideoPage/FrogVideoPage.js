import React from 'react';
import FrogVideo from '../../assets/videos/frog.mp4';
import Footer from '../../Components/Footer/Footer';
import Header from '../../Components/Header/Header';
import Sidebar from '../../Components/Sidebar/Sidebar';
import './FrogVideoPage.css'

const FrogVideoPage = () => {
  return (
    <>
    <Header/>        
    <div className='videopage-container'>
     <Sidebar/>
     <div className='frog-video-wrapper'>
      <video src={FrogVideo} className='frog-video' controls="controls" autoplay="true" />
      <Footer/>
     </div>
    </div>
    </>
  )
}

export default FrogVideoPage
